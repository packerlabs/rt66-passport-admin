# Passport
 ------------
 # How to run this app
1. You need to have nodejs installed
2. Install all dependencies by running the command below
```bash
  cd withNode && npm install
```
3. finally type this:
```bash
  npm start
```
3. In your browser, navigate to http://localhost:3030

4. For livereload while tweaking or hacking this app, you need to   - install nodemon
  ```bash
    npm i -g nodemon
  ```
  - then run this
  ```bash
    npm run startw
  ```
