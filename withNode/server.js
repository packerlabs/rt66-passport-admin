const express = require('express'),
  mongoose = require('mongoose'),
  helmet = require('helmet'),
  router = express.Router(),
  hbs = require('hbs'),
  logger = require('morgan'),
  cloudinary = require('cloudinary'),
  fileParser = require('connect-multiparty')(),
  validator = require('validator'),
  mime = require("mime"),
  path = require('path'),
  config = require('./config.json'),
  bodyParser = require('body-parser'),
  _ = require('lodash'),
  app = express();

mongoose.Promise = global.Promise;

cloudinary.config({cloud_name: config.cloud_name, api_key: config.api_key, api_secret: config.api_secret});

const online_DB_uri = `mongodb://${config.db_user}:${config.db_pass}@ds040167.mlab.com:40167/passport`,
  local_DB_uri = `mongodb://localhost:27017/passport`;

mongoose.connect(online_DB_uri, {
  useMongoClient: true
}, (err, db) => {
  if (err) {
    console.log("Couldn't connect to database");
  } else {
    console.log("Database Connected!");
  }
});

// models
const Location = require('./models/location'),
  User = require('./models/user'),
  Stamp = require('./models/stamp');
  Country = require('./models/country');

/**
   * function fetches all locations available in the database or a few base on the limit supplied, dont supply limit if you want all results
   * @param {limit} maximum no. of result
   * @return [{lon,lat}] the coordinates and other data
   */
const getLocations = (maxresult) => {
  if (!maxresult) {
    return Location
      .find({})
      .exec((err, data) => {
        if (err) {
          console.log(JSON.stringify(err, undefined, 2));
          return err;
        } else {
          return data;
        }
      });
  } else if (validator.isNumeric(maxresult)) {
    return Location
      .find({})
      .limit(maxresult)
      .exec((err, data) => {
        if (err) {
          console.log(JSON.stringify(err, undefined, 2));
          return err;
        } else {
          return data;
        }
      });
  } else {
    return null;
  }
};

/**
   * function fetches all nearby locations available in the database or a few base on the limit supplied, dont supply limit if you want all results
   * @param {Number} Longitude
   * @param {Number} Latitude
   * @return [{lon,lat}]
   */
const nearbyLocations = (long, lat, maxresult) => {
  if (!maxresult) {
    // return Location.find({
    //   loc: {
    //     '$near': [long, lat]
    //   }
    // }).exec((err, data) => {
    //   if (err) {
    //     console.log(JSON.stringify(err, undefined, 2));
    //     return err;
    //   } else {
    //     return data;
    //   }
    // });

    mongoose.connection.db.executeDbCommand({
      geoNear : "locations",  // the mongo collection
      near : [long, lat], // the geo point
      spherical : true,  // tell mongo the earth is round, so it calculates based on a
                         // spherical location system
      distanceMultiplier: 6371, // tell mongo how many radians go into one kilometer.
      maxDistance : 1/6371, // tell mongo the max distance in radians to filter out
    }, function(err, result) {
      if (err) {
        console.log(JSON.stringify(err, undefined, 2));
        return err;
      } else {
        console.log(JSON.stringify(result));
        console.log(result.documents[0].results);
        return result;
      }
    });
  } else if (validator.isNumeric(maxresult)){
    return Location.find({
      loc: {
        '$near': [long, lat]
      }
    }).limit(maxresult).exec((err, data) => {
      if (err) {
        console.log(JSON.stringify(err, undefined, 2));
        return err;
      } else {
        return data;
      }
    });
  }
};

app.use(logger('dev'));
app.use(helmet());
app.disable('x-powered-by');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.set('serverPort', (process.env.PORT || 3030));
process.env.PWD = process.cwd();
app.use(express.static(path.join(process.env.PWD, 'public')));

hbs.registerPartials(path.join(process.env.PWD, 'views/partials'));
app.set('views', path.join(process.env.PWD, 'views'));
app.set('view engine', 'hbs');

hbs.registerHelper('getCurrentYear', () => {
  return new Date().getFullYear();
});

hbs.registerHelper('if_eq', function (a, b, opts) {
  if (a == b) // Or === depending on your needs
    return opts.fn(this);
  else
    return opts.inverse(this);
  }
);

hbs.registerHelper('getPostTime', (timeT) => {
  return new Date(timeT).toDateString();
});

// pages
router.get('/location', (req, res) => {
  res.render('location', {
    title: "Locations"
  });
});

router.get('/stamp', (req, res) => {
  res.render('stamps', {
    title: "Stamps"
  });
});

router.get('/users', (req, res) => {
  res.render('users', {
    title: "Users"
  });
});

router.get('/', (req, res) => {
  res.redirect('/location');
});

// APIs
router.get('/api/location', (req, res) => {
  // fetch all locations
  Location.find({},'', (err, data) => {
    if(!err){
      res.status(200).send(data);
    } else {
      res.send("there was an error fetching data");
    }
  });
});
router.post('/api/location', (req, res) => {
  // fetch all location`
  const rcvdData = req.body;
  if(req.body){
    const newLocation = new Location(rcvdData);
    newLocation.save((err, rez) => {
      if(!err){
        res.send(JSON.stringify(rez, undefined, 2));
      } else {
        console.log(err);
        res.send("there was an error uploading your data");
      }
    });
  }
});
router.patch('/api/location', (req, res) => {
  // fetch all location`
  const rcvdData = req.body;
  console.log(rcvdData);
  res.end("rcvdData");
});

router.get('/api/stamp', (req, res) => {
  // fetch all stamps
  Stamp.find({}, (err, data) => {
    if(!err){
      res.status(200).send(data);
    } else {
      res.send("there was an error fetching data");
    }
  });
});
router.post('/api/stamp', (req, res) => {
  // fetch all stamps
  const rcvdData = req.body;
  if(req.body){
    const newStamp = new Stamp(rcvdData);
    newStamp.save((err, rez) => {
      if(!err){
        res.send(JSON.stringify(rez, undefined, 2));
      } else {
        console.log(err);
        res.send("there was an error uploading your data");
      }
    });
  }
});
router.patch('/api/stamp', (req, res) => {
  // fetch all stamps
  const rcvdData = req.body;
  console.log(rcvdData);
  res.end("rcvdData");
});

router.get('/api/user', (req, res) => {
  // fetch all users
  User.find({}, (err, data) => {
    if(!err) {
      res.status(200).send(data);
    } else {
      res.send("there was an error fetching data");
    }
  });
});
router.post('/api/user', (req, res) => {
  // fetch all users
  const rcvdData = req.body;
  console.log(rcvdData);
  if(rcvdData){
    const newUser = new User(rcvdData);
    newUser.save((err, rez) => {
      if(!err){
        res.send(JSON.stringify(rez, undefined, 2));
      } else {
        console.log(err);
        res.send("there was an error uploading your data");
      }
    });
  }
});
router.put('/api/user', (req, res) => {
  // fetch all users
  const rcvdData = req.body;
  console.log(rcvdData);
  res.end("rcvdData");
});



app.use('/', router);

app.listen(app.get('serverPort'), () => {
  console.log(`Server running at port ${app.get('serverPort')}`);
});