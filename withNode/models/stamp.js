var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var stampSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  display_name: String,
  description: {
    type: String,
    required: false
  },
  icon: String,
  locationId: String,
  website: String,
  createdAt: {
    type: Date,
    required: false
  },
  updatedAt: {
    type: Number,
    required: false
  }
}, {runSettersOnQuery: true});

// userSchema.methods.validatePassword = function (pswd) {
// bcrypt.compareSync(password, pswd);   return this.password === pswd; };

stampSchema.pre('save', function (next) {
  // this.fullname = this.firstname + ' ' + this.lastname;

  var currentDate = new Date().valueOf();
  this.updatedAt = currentDate;
  if (!this.createdAt)
    this.createdAt = currentDate;
  next();
});

// stampSchema.index({ coordinates: '2d' });

var stamp = mongoose.model('stamp', stampSchema);

module.exports = stamp;