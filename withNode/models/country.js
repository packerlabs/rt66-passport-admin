const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const countrySchema = new Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  code: {
    type: String,
    required: true,
    trim: true
  },
  dial_code: {
    type: String,
    required: true,
    trim: true
  }
}, {runSettersOnQuery: true});

const country = mongoose.model('country', countrySchema);

module.exports = country;