var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var locationSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: false
  },
  address: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    required: false
  },
  description: {
    type: String,
    required: false
  },
  longitude: String,
  latitude: String,
  coordinates: {
    type: [String], // longitude, latitude
    index: '2d'
  },
  photos: {
    type: [String]
  },
  sector: Number,
  website: String,
  updatedAt: {
    type: Number,
    required: false
  }
}, {runSettersOnQuery: true});

// userSchema.methods.validatePassword = function (pswd) {
// bcrypt.compareSync(password, pswd);   return this.password === pswd; };

locationSchema.pre('save', function (next) {
  // this.fullname = this.firstname + ' ' + this.lastname;

  var currentDate = new Date().valueOf();
  this.updatedAt = currentDate;
  if (!this.createdAt)
    this.createdAt = currentDate;
  next();
});

// locationSchema.index({ coordinates: '2d' });

var location = mongoose.model('location', locationSchema);

module.exports = location;