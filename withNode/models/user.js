var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  country_name: String,
  country_code:String,
  fullname: String,
  gender: String,
  picture: String,
  address: String,
  email: {
    type: String,
    trim: true,
    unique: true
  },
  latitude: String,
  longitude: String,
  coordinates: {
    type: [String], // longitude, latitude
    index: '2d'
  },
  location: String,
  myrequestpassports: [String],
  mystamps: [String],
  mytrips: [String],
  push_enabled: Boolean,
  createdAt: {
    type: Date,
    required: false
  },
  updatedAt: {
    type: Number,
    required: false
  }
}, {runSettersOnQuery: true});


userSchema.pre('save', function (next) {
  var currentDate = new Date().valueOf();
  this.updatedAt = currentDate;
  if (!this.createdAt)
    this.createdAt = currentDate;
  next();
});

// stampSchema.index({ coordinates: '2d' });

var user = mongoose.model('user', userSchema);

module.exports = user;